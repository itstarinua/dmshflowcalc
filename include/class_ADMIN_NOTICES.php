<?php
/***************************************************
Клас для реализации вывода сообщение в админ. части
by DStaroselskiy 

Version: 0.1
Date: 2016-04-25
****************************************************/
namespace DStaroselskiy\inc;

class ADMIN_NOTICES {
	
	private $admin_notices = array();
	
	public function get_notices(){
		return $this->admin_notices;
	}

	public function add_admin_notices( $msg = Null, $status = 'updated', $id = '', $is_dismissible = true){
		if( empty($msg) || empty($status) ) return false;
		array_push( $this->admin_notices, array( "msg" => $msg , 'status' => $status , 'id' => $id , 'is_dismissible' => $is_dismissible ) );
		
	}
	
	public function show_admin_notices($echo = true){
		$str = '';
		if( count($this->admin_notices) > 0 ) {
			foreach( $this->admin_notices as $admin_notice ){
				$str .='<div id="'.$admin_notice['id'].'" class="'.$admin_notice['status'].' notice '.( ( $admin_notice['is_dismissible'] == true ) ? 'is-dismissible' : '').'"><p>'.$admin_notice['msg'].'</p>'.( ( $admin_notice['is_dismissible'] == true ) ? '<button type="button" class="notice-dismiss"><span class="screen-reader-text">'.__('Скрыть это уведомление.','dms_plugin').'</span></button>' : '').'</div>' . PHP_EOL;
			}
		}
		if( $echo ) echo $str;
		return $str;
	}
	public function print_admin_notices(){
		$this->show_admin_notices(true);		
	}
	
	public function admin_notices_init(){
		if( !isset($this->admin_page_id) ) return false;
		$current_screen = \get_current_screen();		
		if( $this->admin_page_id != $current_screen->base) return false;
		\add_action( 'all_admin_notices', array( &$this, 'print_admin_notices') );
	}
	
	function __construct() {
		\add_action('admin_head', array( &$this, 'admin_notices_init'),100,0 );
	}
	
}
?>