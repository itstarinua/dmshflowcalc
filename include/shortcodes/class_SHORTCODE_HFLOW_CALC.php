<?php
/*****************************************************************
Трей содержащий функционал шорткода вывода и обработки формы входа 
для пользователей имеющих профиль
*****************************************************************/
namespace DStaroselskiy\Shortcodes;

if( !class_exists('DStaroselskiy\Shortcodes\SHORTCODE_HFLOW_CALC') ) {
	if( !class_exists( '\DStaroselskiy\inc\ADMIN_NOTICES' ) ) require_once( CATALOG_OF_PROFILES_DIR."/include/class_ADMIN_NOTICES.php");

	class SHORTCODE_HFLOW_CALC extends \DStaroselskiy\inc\ADMIN_NOTICES {
		protected $form_nonce_fild = "dms-hflow-calc-login";
		protected $form_nonce_action = "dms-hflow-calc-action";	
		
		public function print_shortcode(){
			if( file_exists( HFLOW_CALCULATOR_DIR.'tpl/content.tpl' ) ) {
				global $DMS_h_flow_calculator;
				$options = $DMS_h_flow_calculator->get_options();
				$content = file_get_contents( HFLOW_CALCULATOR_DIR.'tpl/content.tpl');	
				echo str_replace(
					array(
						'*|WP_NONCE_FIELD|*', 
						'*|HFLOW_CALCULETOR_BEFORE|*',
						'*|THE_REDUCTION_IN_THE_FREQUENCY_OF_REVERSE_FLUSHING|*',
						'*|REDUCING_THE_TIME_A_REVERSE_LEACHING|*',
						'*||*',
					),
					array(
						\wp_nonce_field( $this->form_nonce_action, $this->form_nonce_fild, true , false ),
						'',
						$options["coefficients"]["the_reduction_in_the_frequency_of_reverse_flushing"],
						$options["coefficients"]["reducing_the_time_a_reverse_leaching"],
						'',
					),
					$content
				);
				\add_action('wp_print_footer_scripts', array( &$this, 'print_in_footer'), 20, 0);
			}
		}
	
		public function print_in_footer(){
		$url = untrailingslashit( HFLOW_CALCULATOR_URL );
		echo <<<EOF
		<link rel='stylesheet' type='text/css' href='$url/css/core.css'/>		
		<script type='text/javascript' src='$url/js/core.js'></script>
EOF;
		}
		public function	send_mail(){
			if( !isset($_POST[ $this->form_nonce_fild ] ) ) return false;
			if( !wp_verify_nonce($_POST[ $this->form_nonce_fild ], $this->form_nonce_action ) ) {	return false; }
			global $DMS_h_flow_calculator;
			$options = $DMS_h_flow_calculator->get_options();
			if($options["mail"]["power_off"] != "true") {
				$this->add_admin_notices( __('Отправка запросов временно отвлючена.' , 'dms_plugin'), 'error', '', false);				
			}else if( file_exists( HFLOW_CALCULATOR_DIR.'tpl/mail.tpl' ) ) {
				$is_error = false;
				if( !isset($_POST['user']['name']) ) {
					$this->add_admin_notices( __('Заполниет email.' , 'dms_plugin'), 'error', 'name', false);
					$is_error = true;
				}else if( $_POST['user']['name'] == "" ) {
					$this->add_admin_notices( __('Заполниет email.' , 'dms_plugin'), 'error', 'name', false);
					$is_error = true;
				}
				if( !isset($_POST['user']['phone']) ) {
					$this->add_admin_notices( __('Заполниет номер телефона.' , 'dms_plugin'), 'error', 'phone', false);
					$is_error = true;
				}else if( $_POST['user']['phone'] == "" ) {
					$this->add_admin_notices( __('Заполниет номер телефона.' , 'dms_plugin'), 'error', 'phone', false);
					$is_error = true;
				}
				if( !isset($_POST['user']['email']) ) {
					$this->add_admin_notices( __('Заполниет email.' , 'dms_plugin'), 'error', 'email', false);
					$is_error = true;
				}else if( $_POST['user']['email'] == "" ) {
					$this->add_admin_notices( __('Заполниет email.' , 'dms_plugin'), 'error', 'email', false);
					$is_error = true;
				}else if( !is_email($_POST['user']['email']) ) {
					$this->add_admin_notices( __('Введите корректный email.' , 'dms_plugin'), 'error', 'email', false);
					$is_error = true;
				}
				if( $is_error ) {
					$this->add_admin_notices( __('Данные не отправлены. Пожалуйста заполните все поля.' , 'dms_plugin'), 'error', '', false);
				}else{
					
					
					$body = file_get_contents( HFLOW_CALCULATOR_DIR.'tpl/mail.tpl');	
					$body = str_replace(
						array(
							'*|MAIL_TITLE|*', 
							'*|MAIL_TITLE_DATE|*',
							'*|USER_NAME|*',
							'*|USER_EMAIL|*',
							'*|USER_PHONE|*',
							'*|LENGTH|*',
							'*|WIDTH|*',
							'*|DIP|*',						
						),
						array(
							$options["mail"]["title"],
							$options["mail"]["title"].' от '.date('d-m-Y'),
							esc_html($_POST['user']['name']),
							esc_html($_POST['user']['email']),
							esc_html($_POST['user']['phone']),
							esc_html($_POST['parametry']['length']),
							esc_html($_POST['parametry']['width']),
							esc_html($_POST['parametry']['dip']),
						),
						$body
					);
					
					if( isset($_POST['hflow_calculetor_input_data']) 
						&& is_array($_POST['hflow_calculetor_input_data'])
					) foreach($_POST['hflow_calculetor_input_data'] as $key => $value ) {
						$body = str_replace( '*|'.$key.'|*', $value, $body);
					}
					
					if( isset($_POST['hflow_calculetor_calculation_data']) 
						&& is_array($_POST['hflow_calculetor_calculation_data'])
					) foreach($_POST['hflow_calculetor_calculation_data'] as $key => $value ) {
						$body = str_replace( '*|'.$key.'|*', $value, $body);
					}
					$headers = array( 'From: '.$options["mail"]['send_from'], 'content-type: text/html');
					\add_filter( 'wp_mail_from_name', array( &$this, 'mail_from_name' ) );
					if( wp_mail( $options["mail"]['send_to'], $options["mail"]["title"], $body, $headers) ) {
						$this->add_admin_notices( __('Данные отпралены.' , 'dms_plugin'), 'updated', '', false);
						die( json_encode( array( "status" => "ok", "msg" => $this->get_notices() ) ) );
					}else{
						$this->add_admin_notices( __('Ошибка отправки письма. Попробуйте позже.' , 'dms_plugin'), 'error', '', false);
					}
				}
			}else{
				$this->add_admin_notices( __('Файл тела письма не найден. Обратитесь к администраору.' , 'dms_plugin'), 'error', '', false);				
			}
			die( json_encode( array( "status" => "error", "msg" => $this->get_notices() ) ) );
		}
			
		function mail_from_name( $email_from ){
			return get_option('blogname');
		}
		
		function __construct() {
			\add_shortcode('dms-hflow-calc', array( &$this, 'print_shortcode'));
			\add_action('init', array( &$this, 'send_mail'), 1000, 0);
		}
	}

	global $DMS_shortcode_hflow_calc;
	$DMS_shortcode_hflow_calc = new SHORTCODE_HFLOW_CALC();
}	
?>