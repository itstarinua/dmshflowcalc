<form id="hflow_calculetor" action="" method="POST">
	*|WP_NONCE_FIELD|*
	*|HFLOW_CALCULETOR_BEFORE|*
	<table id="hflow_calculetor_input_data">
		<thead>
			<tr>
				<td colspan="2">Параметры бассейна и постоянные эксплуатационные расходы</td>
			</tr>
		</thead>
		<tbody>
			<tr class="characteristic">
				<td>Производительность насоса  M<sup>3</sup> /  мин</td>
				<td><input name="hflow_calculetor_input_data[pump_output]" id="pump_output" value="5" /></td>
			</tr>
			<tr class="characteristic">
				<td>Частота обратной промывки в неделю</td>
				<td><input name="hflow_calculetor_input_data[backwashing_per_week]" id="backwashing_per_week" value="1.5" /></td>
			</tr>
			<tr class="characteristic">
				<td>Время обратной промывки (мин)</td>
				<td><input name="hflow_calculetor_input_data[time_of_backwash]" id="time_of_backwash" value="10"/></td>
			</tr>
			<tr class="characteristic">
				<td>Температура подаваемой воды</td>
				<td><input name="hflow_calculetor_input_data[temperature_of_the_feedwater]" id="temperature_of_the_feedwater" value="10"/></td>
			</tr>
			<tr class="characteristic">
				<td>Температура бассейна</td>
				<td><input name="hflow_calculetor_input_data[pool_temperature]" id="pool_temperature" value="29"/></td>
			</tr>
			<tr class="characteristic">
				<td>Стоимость электроэнергии / кВт час</td>
				<td><input name="hflow_calculetor_input_data[cost_of_electricity]" id="cost_of_electricity" value="0.97"/><span></span></td>
			</tr>
			<tr class="characteristic">
				<td>Стоимость воды / M<sup>3</sup></td>
				<td><input name="hflow_calculetor_input_data[cost_of_woter]" id="cost_of_woter" value="4.6"/><span></span></td>
			</tr>
			<tr class="characteristic">
				<td>Стоимость  хлора / год</td>
				<td><input name="hflow_calculetor_input_data[cost_of_chlorine]" id="cost_of_chlorine" value="500"/><span></span></td>
			</tr>
			<tr class="characteristic">
				<td>Стоимость кислоты / год</td>
				<td><input name="hflow_calculetor_input_data[cost_of_acid]" id="cost_of_acid" value="500"/><span></span></td>
			</tr>
			<tr class="characteristic">
				<td>Стоимость коагулянта / год</td>
				<td><input name="hflow_calculetor_input_data[cost_of_coagulant]" id="cost_of_coagulant" value="500"/><span></span></td>
			</tr>
			<tr class="characteristic">
				<td>Расходы на канализационно_очистительную систему за M<sup>3</sup></td>
				<td><input name="hflow_calculetor_input_data[cost_of_the_sewer_cleaning_system]" id="cost_of_the_sewer_cleaning_system" value="4.1"/><span></span></td>
			</tr>
			<tr class="characteristic">
				<td>Озонирование или УФ номинальная нагрузка / кВт час</td>
				<td><input name="hflow_calculetor_input_data[Ozonation_or_UV_rated_load]" id="Ozonation_or_UV_rated_load" value="0.60"/></td>
			</tr>
			<tr class="characteristic">
				<td>Электричество для озонирования или УФ / кВт час</td>
				<td><input name="hflow_calculetor_input_data[Electricity_for_ozonation_or_UV]" id="Electricity_for_ozonation_or_UV" value="0.97"/><span></span></td>
			</tr>
			<tr class="characteristic">
				<td>Тех. Эксплуатация озона или УФ час / день</td>
				<td><input name="hflow_calculetor_input_data[Those_Operation_of_ozone_or_UV]" id="Those_Operation_of_ozone_or_UV" value="24"/></td>
			</tr>
		</tfoot>
	</table>	
	<div id="hflow_calculetor_calculation_container">
		<button id="hflow_calculetor_calculation_start" type="button">Рассчитать</button>
		<table id="hflow_calculetor_calculation_data" class="dms-hidden">
			<thead>
				<tr id="amount_of_expenses_per_year">
					<td><b>Сумма расходов в год</b></td>
					<td><b>До <i>Aqua</i></b>KLEAR</td>
					<td><b>После <i>Aqua</i></b>KLEAR</td>
				</tr>
			</thead>
			<tbody class="dms-hidden">
				<tr>
					<td>Сокращение частоты обратных промывок</td>
					<td colspan="2"><input readonly="readonly"  name="hflow_calculetor_input_data[the_reduction_in_the_frequency_of_reverse_flushing]" id="the_reduction_in_the_frequency_of_reverse_flushing" value="*|THE_REDUCTION_IN_THE_FREQUENCY_OF_REVERSE_FLUSHING|*"/></td>
				</tr>
				<tr>
					<td>Сокращение времение обратных промывок</td>
					<td colspan="2"><input readonly="readonly" name="hflow_calculetor_input_data[reducing_the_time_a_reverse_leaching]" id="reducing_the_time_a_reverse_leaching" value="*|REDUCING_THE_TIME_A_REVERSE_LEACHING|*"/></td>
				</tr>
				<tr class="amount_of_expenses_per_year">
					<td>Производительность насоса  M<sup>3</sup> /  мин</td>
					<td><input readonly="readonly" name="hflow_calculetor_calculation_data[do_pump_output]" id="do_pump_output" value="2.5" /></td>
					<td><input readonly="readonly" name="hflow_calculetor_calculation_data[after_pump_output]" id="after_pump_output" value="5" /></td>
				</tr>
				<tr class="amount_of_expenses_per_year">
					<td>Частота обратной промывки в неделю</td>
					<td><input readonly="readonly" name="hflow_calculetor_calculation_data[do_backwashing_per_week]" id="do_backwashing_per_week"><span></span></td>
					<td><input readonly="readonly" name="hflow_calculetor_calculation_data[after_backwashing_per_week]" id="after_backwashing_per_week"><span></span></td>
				</tr>
				<tr class="amount_of_expenses_per_year">
					<td>Интервал между обратными промывками в неделях</td>
					<td><input readonly="readonly" name="hflow_calculetor_calculation_data[do_interval_between_back_flushes_in_weeks]" id="do_interval_between_back_flushes_in_weeks"><span></span></td>
					<td><input readonly="readonly" name="hflow_calculetor_calculation_data[after_interval_between_back_flushes_in_weeks]" id="after_interval_between_back_flushes_in_weeks"><span></span></td>
				</tr>
				<tr class="amount_of_expenses_per_year">
					<td>Частота обратных промывок / год</td>
					<td><input readonly="readonly" name="hflow_calculetor_calculation_data[do_return_the_frequency_of_washes]" id="do_return_the_frequency_of_washes"><span></span></td>
					<td><input readonly="readonly" name="hflow_calculetor_calculation_data[after_return_the_frequency_of_washes]" id="after_return_the_frequency_of_washes"><span></span></td>
				</tr>
				<tr class="amount_of_expenses_per_year">
					<td>Время обратной промывки (мин)</td>
					<td><input readonly="readonly" name="hflow_calculetor_calculation_data[do_time_of_backwash]" id="do_time_of_backwash"><span></span></td>
					<td><input readonly="readonly" name="hflow_calculetor_calculation_data[after_time_of_backwash]" id="after_time_of_backwash"><span></span></td>
				</tr>
				<tr class="amount_of_expenses_per_year">
					<td>M<sup>3</sup> на обратную промывку</td>
					<td><input readonly="readonly" name="hflow_calculetor_calculation_data[do_m3_for_back_washing]" id="do_m3_for_back_washing"><span></span></td>
					<td><input readonly="readonly" name="hflow_calculetor_calculation_data[after_m3_for_back_washing]" id="after_m3_for_back_washing"><span></span></td>
				</tr>
				<tr class="amount_of_expenses_per_year">
					<td>Подогрев воды для обратной промывки</td>
					<td><input readonly="readonly" name="hflow_calculetor_calculation_data[do_heating_water_for_backwashing]" id="do_heating_water_for_backwashing"><span></span></td>
					<td><input readonly="readonly" name="hflow_calculetor_calculation_data[after_heating_water_for_backwashing]" id="after_heating_water_for_backwashing"><span></span></td>
				</tr>
				<tr class="amount_of_expenses_per_year">
					<td>Нормированная вода (водоснабжение)</td>
					<td><input readonly="readonly" name="hflow_calculetor_calculation_data[do_normalized_water]" id="do_normalized_water"><span></span></td>
					<td><input readonly="readonly" name="hflow_calculetor_calculation_data[afre_normalized_water]" id="afre_normalized_water"><span></span></td>
				</tr>
				<tr class="amount_of_expenses_per_year">
					<td>Расходы на канализационно-очистительную систему</td>
					<td><input readonly="readonly" name="hflow_calculetor_calculation_data[do_cost_of_the_sewer_cleaning_system]" id="do_cost_of_the_sewer_cleaning_system"><span></span></td>
					<td><input readonly="readonly" name="hflow_calculetor_calculation_data[after_cost_of_the_sewer_cleaning_system]" id="after_cost_of_the_sewer_cleaning_system"><span></span></td>
				</tr>
				<tr class="amount_of_expenses_per_year">
					<td>Хлор</td>
					<td><input readonly="readonly" name="hflow_calculetor_calculation_data[do_chlorine]" id="do_chlorine"><span></span></td>
					<td><input readonly="readonly" name="hflow_calculetor_calculation_data[after_chlorine]" id="after_chlorine"><span></span></td>
				</tr>
				<tr class="amount_of_expenses_per_year">
					<td>Кислота</td>
					<td><input readonly="readonly" name="hflow_calculetor_calculation_data[do_acid]" id="do_acid"><span></span></td>
					<td><input readonly="readonly" name="hflow_calculetor_calculation_data[after_acid]" id="after_acid"><span></span></td>
				</tr>
				<tr class="amount_of_expenses_per_year">
					<td>Коагулянт</td>
					<td><input readonly="readonly" name="hflow_calculetor_calculation_data[do_coagulant]" id="do_coagulant"><span></span></td>
					<td><input readonly="readonly" name="hflow_calculetor_calculation_data[after_coagulant]" id="after_coagulant"><span></span></td>
				</tr>
				<tr class="amount_of_expenses_per_year">
					<td>Дозирование озона или УФ</td>
					<td><input readonly="readonly" name="hflow_calculetor_calculation_data[do_dosing_of_ozone_or_UV]" id="do_dosing_of_ozone_or_UV"><span></span></td>
					<td><input readonly="readonly" name="hflow_calculetor_calculation_data[after_dosing_of_ozone_or_UV]" id="after_dosing_of_ozone_or_UV"><span></span></td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td><b>Всего стоимость  / год</b></td>
					<td><b><input readonly="readonly" name="hflow_calculetor_calculation_data[do_total_cost_year]" id="do_total_cost_year"><span></span></b></td>
					<td><b><input readonly="readonly" name="hflow_calculetor_calculation_data[after_total_cost_year]" id="after_total_cost_year"><span></span></b></td>
				</tr>
				<tr>
					<td><b>Общая экономия</b></td>
					<td colspan="2"><b><input readonly="readonly" name="hflow_calculetor_calculation_data[total_savings]" id="total_savings"><span></span></b></td>
				</tr>
			</tfoot>
		</table>
	</div>
	<div id="send_data" class="dms-hidden">
		<button id="hflow_calculetor_send" type="button">Оформить заявку на установку тестового прибора</button>
		<div id="contsct-form" class="dms-hidden">
			<h3>Параметры Вашего бассейна</h3>
			<div id="parametry">
				<label><span>Длинна, м</span> <input name="parametry[length]"></label>
				<label><span>Ширина, м</span> <input name="parametry[width]"></label>
				<label><span>Глубина, м</span> <input name="parametry[dip]"></label>
			</div>
			<h3>Ваши контактные данные</h3>
			<div id="contact-date">
				<span><input required name="user[name]" placeholder="*Имя"></span>
				<span><input required name="user[email]" placeholder="*E-mail"></span>
				<span><input required name="user[phone]" placeholder="*Телефон"></span>
			</div>
			<div id="form-report"></div>
			<button id="hflow_submit" type="submit">Отправить</button>
		</div>
	</div>
</form>
