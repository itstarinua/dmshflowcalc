<div class="wrap">
	<h1>*|PAGE_TITLE|*</h1>
	<form method="post" enctype="multipart/form-data" action="" id="">
		*|WP_NONCE_FIELD|*
		<table class="form-table">
			<tbody>
				*|HFLOW_CALCULATOR_BEFORE|*
				<tr>
					<th scope="row" colspan="2">
						<h2>*|HFLOW_CALCULATOR_MAIL_BLOCK_TITLE|*</h2>
					</th>
				</tr>
				<tr>
					<th scope="row" id="hflow-calculator-mail-power-off-title">
						*|HFLOW_CALCULATOR_MAIL_POWER_OFF_TITLE|*
					</th>
					<td>
						<label><input name="mail[power_off]" id="hflow-calculator-mail-power-off-true" value="true" *|HFLOW_CALCULATOR_MAIL_POWER_OFF_TRUE_CHECKED|* class="" type="radio">*|HFLOW_CALCULATOR_MAIL_POWER_OFF_TRUE_TEXT|*</label><br>
						<label><input name="mail[power_off]" id="hflow-calculator-mail-power-off-false" value="false" *|HFLOW_CALCULATOR_MAIL_POWER_OFF_FALSE_CHECKED|* class="" type="radio">*|HFLOW_CALCULATOR_MAIL_POWER_OFF_FALSE_TEXT|*</label>
					</td>
				</tr>
				<tr>
					<th scope="row" id="hflow-calculator-mail-title">
						*|HFLOW_CALCULATOR_MAIL_TITLE_TEXT|*
					</th>
					<td>
						<input style="width: 100%;" name="mail[title]" id="mail-title" value="*|HFLOW_CALCULATOR_MAIL_TITLE|*" type="text">
					</td>
				</tr>
				<tr>
					<th scope="row" id="hflow-calculator-mail-send-to-title">
						*|HFLOW_CALCULATOR_MAIL_SEND_TO_TEXT|*
					</th>
					<td>
						<input style="width: 100%;" name="mail[send_to]" id="hflow-calculator-mail-send-to" value="*|HFLOW_CALCULATOR_MAIL_SEND_TO|*" type="text"></br>
						<p>*|HFLOW_CALCULATOR_MAIL_SEND_TO_DESCRIPTION|*</p>
					</td>
				</tr>
				<tr>
					<th scope="row" id="hflow-calculator-mail-send-from-title">
						*|HFLOW_CALCULATOR_MAIL_SEND_FROM_TEXT|*
					</th>
					<td>
						<input style="width: 100%;" name="mail[send_from]" id="hflow-calculator-mail-send-from" value="*|HFLOW_CALCULATOR_MAIL_SEND_FROM|*" type="text"></br>
						<p>*|HFLOW_CALCULATOR_MAIL_SEND_FROM_DESCRIPTION|*</p>
					</td>
				</tr>
				<tr>
					<th scope="row" colspan="2">
						<h2>*|HFLOW_CALCULATOR_COEFFICIENTS_TITLE|*</h2>
					</th>
				</tr>
				<tr>
					<th scope="row" id="the_reduction_in_the_frequency_of_reverse_flushing-title">
						*|THE_REDUCTION_IN_THE_FREQUENCY_OF_REVERSE_FLUSHING_TEXT|*
					</th>
					<td>
						<input style="width: 100%;" name="coefficients[the_reduction_in_the_frequency_of_reverse_flushing]" id="mail-title" value="*|THE_REDUCTION_IN_THE_FREQUENCY_OF_REVERSE_FLUSHING|*" type="text">
					</td>
				</tr>
				<tr>
					<th scope="row" id="reducing_the_time_a_reverse_leaching-title">
						*|REDUCING_THE_TIME_A_REVERSE_LEACHING_TEXT|*
					</th>
					<td>
						<input style="width: 100%;" name="coefficients[reducing_the_time_a_reverse_leaching]" id="mail-title" value="*|REDUCING_THE_TIME_A_REVERSE_LEACHING|*" type="text">
					</td>
				</tr>

				*|HFLOW_CALCULATOR_AFTER|*
			</tbody>
		</table>	
		*|SUBMIT_BUTTON|*
	</form>
</div>