<!DOCTYPE html>
<html>
	<head>		
		<meta charset="utf-8"/>
		<meta name="viewport" content="width=device-width"/>
		<meta content="DStaroselskiy (IT-Star company)" name="author" />
		<meta content="noindex,nofollow" name="Robots"/>		
		<title>*|MAIL_TITLE|*</title>
	</head>
	<body>
		<h1>*|MAIL_TITLE_DATE|*</h1>
		<hr>
		<h2>Данные пользователя</h2>
		<p><b>Имя:</b> *|USER_NAME|*</p>
		<p><b>E-MAIL:</b> *|USER_EMAIL|*</p>
		<p><b>Телефон:</b> *|USER_PHONE|*</p>
		<hr/>
		<h2>Габариты бассейна</h2>
		<p><b>Длинна, м</b> *|LENGTH|*</p>
		<p><b>Ширина, м</b> *|WIDTH|*</p>
		<p><b>Глубина, м</b> *|DIP|*</p>
		<hr/>
		<h2>Параметры бассейна и постоянные эксплуатационные расходы</h2>
		<table>
			<tbody>
				<tr class="characteristic">
					<td>Производительность насоса  M<sup>3</sup> /  мин</td>
					<td>*|pump_output|*</td>
				</tr>
				<tr class="characteristic">
					<td>Частота обратной промывки в неделю</td>
					<td>*|backwashing_per_week|*</td>
				</tr>
				<tr class="characteristic">
					<td>Время обратной промывки (мин)</td>
					<td>*|time_of_backwash|*</td>
				</tr>
				<tr class="characteristic">
					<td>Температура подаваемой воды</td>
					<td>*|temperature_of_the_feedwater|*</td>
				</tr>
				<tr class="characteristic">
					<td>Температура бассейна</td>
					<td>*|pool_temperature|*</td>
				</tr>
				<tr class="characteristic">
					<td>Стоимость электроэнергии / кВт час</td>
					<td>*|cost_of_electricity|*</td>
				</tr>
				<tr class="characteristic">
					<td>Стоимость воды / M<sup>3</sup></td>
					<td>*|cost_of_woter|*</td>
				</tr>
				<tr class="characteristic">
					<td>Стоимость  хлора / год</td>
					<td>*|cost_of_chlorine|*</td>
				</tr>
				<tr class="characteristic">
					<td>Стоимость кислоты / год</td>
					<td>*|cost_of_acid|*</td>
				</tr>
				<tr class="characteristic">
					<td>Стоимость коагулянта / год</td>
					<td>*|cost_of_coagulant|*</td>
				</tr>
				<tr class="characteristic">
					<td>Расходы на канализационно_очистительную систему за M<sup>3</sup></td>
					<td>*|cost_of_the_sewer_cleaning_system|*</td>
				</tr>
				<tr class="characteristic">
					<td>Озонирование или УФ номинальная нагрузка / кВт час</td>
					<td>*|Ozonation_or_UV_rated_load|*</td>
				</tr>
				<tr class="characteristic">
					<td>Электричество для озонирования или УФ / кВт час</td>
					<td>*|Electricity_for_ozonation_or_UV|*</td>
				</tr>
				<tr class="characteristic">
					<td>Тех. Эксплуатация озона или УФ час / день</td>
					<td>*|Those_Operation_of_ozone_or_UV|*</td>
				</tr>
			</tbody>
		</table>
		<hr/>
		<h2>РАССЧЕТЫ</h2>
		<table id="hflow_calculetor_calculation_data" class="dms-hidden">
			<thead>
				<tr id="amount_of_expenses_per_year">
					<td><b>Сумма расходов в год</b></td>
					<td><b>До <i>Aqua</i></b>KLEAR</td>
					<td><b>После <i>Aqua</i></b>KLEAR</td>
				</tr>
			</thead>
			<tbody class="dms-hidden">
				<tr>
					<td>Сокращение частоты обратных промывок</td>
					<td colspan="2">*|the_reduction_in_the_frequency_of_reverse_flushing|*</td>
				</tr>
				<tr>
					<td>Сокращение времение обратных промывок</td>
					<td colspan="2">*|reducing_the_time_a_reverse_leaching|*</td>
				</tr>
				<tr class="amount_of_expenses_per_year">
					<td>Производительность насоса  M<sup>3</sup> /  мин</td>
					<td>*|do_pump_output|*</td>
					<td>*|after_pump_output|*</td>
				</tr>
				<tr class="amount_of_expenses_per_year">
					<td>Частота обратной промывки в неделю</td>
					<td>*|do_backwashing_per_week|*</td>
					<td>*|after_backwashing_per_week|*</td>
				</tr>
				<tr class="amount_of_expenses_per_year">
					<td>Интервал между обратными промывками в неделях</td>
					<td>*|do_interval_between_back_flushes_in_weeks|*</td>
					<td>*|after_interval_between_back_flushes_in_weeks|*</td>
				</tr>
				<tr class="amount_of_expenses_per_year">
					<td>Частота обратных промывок / год</td>
					<td>*|do_return_the_frequency_of_washes|*</td>
					<td>*|after_return_the_frequency_of_washes|*</td>
				</tr>
				<tr class="amount_of_expenses_per_year">
					<td>Время обратной промывки (мин)</td>
					<td>*|do_time_of_backwash|*</td>
					<td>*|after_time_of_backwash|*</td>
				</tr>
				<tr class="amount_of_expenses_per_year">
					<td>M<sup>3</sup> на обратную промывку</td>
					<td>*|do_m3_for_back_washing|*</td>
					<td>*|after_m3_for_back_washing|*</td>
				</tr>
				<tr class="amount_of_expenses_per_year">
					<td>Подогрев воды для обратной промывки</td>
					<td>*|do_heating_water_for_backwashing|*</td>
					<td>*|after_heating_water_for_backwashing|*</td>
				</tr>
				<tr class="amount_of_expenses_per_year">
					<td>Нормированная вода (водоснабжение)</td>
					<td>*|do_normalized_water|*</td>
					<td>*|afre_normalized_water|*</td>
				</tr>
				<tr class="amount_of_expenses_per_year">
					<td>Расходы на канализационно-очистительную систему</td>
					<td>*|do_cost_of_the_sewer_cleaning_system|*</td>
					<td>*|after_cost_of_the_sewer_cleaning_system|*</td>
				</tr>
				<tr class="amount_of_expenses_per_year">
					<td>Хлор</td>
					<td>*|do_chlorine|*</td>
					<td>*|after_chlorine|*</td>
				</tr>
				<tr class="amount_of_expenses_per_year">
					<td>Кислота</td>
					<td>*|do_acid|*</td>
					<td>*|after_acid|*</td>
				</tr>
				<tr class="amount_of_expenses_per_year">
					<td>Коагулянт</td>
					<td>*|do_coagulant|*</td>
					<td>*|after_coagulant|*</td>
				</tr>
				<tr class="amount_of_expenses_per_year">
					<td>Дозирование озона или УФ</td>
					<td>*|do_dosing_of_ozone_or_UV|*</td>
					<td>*|after_dosing_of_ozone_or_UV|*</td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td><b>Всего стоимость  / год</b></td>
					<td><b>*|do_total_cost_year|*</b></td>
					<td><b>*|after_total_cost_year|*</b></td>
				</tr>
				<tr>
					<td><b>Общая экономия</b></td>
					<td colspan="2"><b>*|total_savings|*</b></td>
				</tr>
			</tfoot>
		</table>
		<hr/>
		<h6>Письмо сгенирировано автомапически плагином H-Flow Calculator</h6>
	</body>
</html>
