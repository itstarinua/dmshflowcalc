(function( $ ) {
	jQuery(document).ready(function(){
		window.calc = {};
		function calc_get_data(){
			jQuery('#hflow_calculetor_input_data input, #the_reduction_in_the_frequency_of_reverse_flushing, #reducing_the_time_a_reverse_leaching').each(function(){
				id = jQuery(this).attr('id');
				if( id.length == 0) return;
				val = jQuery(this).val().replace(',','.');				
				if( val.length > 0) {
					val = parseFloat( val );
					if( isNaN(val) ) val = 0;
				}else{
					val = 0;
				}			
				window.calc[id] = val;
			});
		}
		
		function  print_float( data ){
			data_int = parseInt(data);
			data_float = Math.round( ( data - data_int ) * 100 );
			if( data_float  < 0 ) data_float = 0 - data_float;
			return data_int+'.'+(( data_float < 10 ) ? '0' : '')+data_float ;
		}
		
		function calc_do_ustanovki(){
			window.calc.do_total_cost_year = 0;
			//==== Производительность насоса ==========
			window.calc.do_pump_output = ( typeof window.calc.pump_output == 'undefined') ? 0 : window.calc.pump_output;
			jQuery('#do_pump_output').val(  window.calc.do_pump_output.toFixed(2));
			
			//==== Частота обратной промывки в неделю ==========
			window.calc.do_backwashing_per_week = ( typeof window.calc.backwashing_per_week == 'undefined') ? 0 : window.calc.backwashing_per_week;
			jQuery('#do_backwashing_per_week').val(  window.calc.do_backwashing_per_week.toFixed(2));
			//==== Интервал между обратными промывками в неделях ==========
			window.calc.do_interval_between_back_flushes_in_weeks = (( typeof window.calc.do_backwashing_per_week == 'undefined') || ( window.calc.do_backwashing_per_week == 0 ) ) ? 0 : ( 1 / window.calc.do_backwashing_per_week );
			jQuery('#do_interval_between_back_flushes_in_weeks').val(  window.calc.do_interval_between_back_flushes_in_weeks.toFixed(2));
			//==== Частота обратных промывок / год ==========
			window.calc.do_return_the_frequency_of_washes = ( typeof window.calc.backwashing_per_week == 'undefined') ? 0 : ( window.calc.backwashing_per_week * 49 );
			jQuery('#do_return_the_frequency_of_washes').val(  window.calc.do_return_the_frequency_of_washes.toFixed(2));
			//==== Время обратной промывки (мин) ==========
			window.calc.do_time_of_backwash = ( typeof window.calc.time_of_backwash == 'undefined') ? 0 : window.calc.time_of_backwash;
			jQuery('#do_time_of_backwash').val( window.calc.do_time_of_backwash.toFixed(2) );
			//==== M³ на обратную промывку ==========
			window.calc.do_m3_for_back_washing = (( typeof window.calc.do_pump_output == 'undefined') || ( typeof window.calc.do_time_of_backwash == 'undefined') ) ? 0 : (window.calc.do_pump_output * window.calc.do_time_of_backwash);
			jQuery('#do_m3_for_back_washing').val(  window.calc.do_m3_for_back_washing.toFixed(2));
			//==== Подогрев воды для обратной промывки ==========
			window.calc.do_heating_water_for_backwashing = 
			(( typeof window.calc.do_m3_for_back_washing == 'undefined') 
				|| ( typeof window.calc.do_return_the_frequency_of_washes == 'undefined') 
				|| ( typeof window.calc.cost_of_electricity == 'undefined') 
			) ? 0 : ( window.calc.do_m3_for_back_washing * 1000000 * ( ( ( typeof window.calc.pool_temperature == 'undefined') ? 0 : window.calc.pool_temperature )  - ( ( typeof window.calc.temperature_of_the_feedwater == 'undefined') ? 0 : window.calc.temperature_of_the_feedwater) ) * window.calc.do_return_the_frequency_of_washes * window.calc.cost_of_electricity / 859845.2  );
			jQuery('#do_heating_water_for_backwashing').val(  window.calc.do_heating_water_for_backwashing.toFixed(2));
			window.calc.do_total_cost_year += window.calc.do_heating_water_for_backwashing;
			//==== Нормированная вода (водоснабжение) ==========
			window.calc.do_normalized_water = 
			(( typeof window.calc.do_m3_for_back_washing == 'undefined') 
				|| ( typeof window.calc.cost_of_woter == 'undefined') 
				|| ( typeof window.calc.do_return_the_frequency_of_washes == 'undefined') 
			) ? 0 : ( window.calc.do_m3_for_back_washing * window.calc.cost_of_woter * window.calc.do_return_the_frequency_of_washes);
			jQuery('#do_normalized_water').val(  window.calc.do_normalized_water.toFixed(2));
			window.calc.do_total_cost_year += window.calc.do_normalized_water;
			//==== Расходы на канализационно_очистительную систему ==========
			window.calc.do_cost_of_the_sewer_cleaning_system = 
			(( typeof window.calc.do_m3_for_back_washing == 'undefined') 
				|| ( typeof window.calc.cost_of_the_sewer_cleaning_system == 'undefined') 
				|| ( typeof window.calc.do_return_the_frequency_of_washes == 'undefined') 
			) ? 0 : ( window.calc.do_m3_for_back_washing * window.calc.cost_of_the_sewer_cleaning_system * window.calc.do_return_the_frequency_of_washes);
			jQuery('#do_cost_of_the_sewer_cleaning_system').val(  window.calc.do_cost_of_the_sewer_cleaning_system.toFixed(2));
			window.calc.do_total_cost_year += window.calc.do_cost_of_the_sewer_cleaning_system;
			//==== Хлор ==========
			window.calc.do_chlorine = ( typeof window.calc.cost_of_chlorine == 'undefined') ? 0 : window.calc.cost_of_chlorine;
			jQuery('#do_chlorine').val(  window.calc.do_chlorine.toFixed(2) );
			window.calc.do_total_cost_year += window.calc.do_chlorine;
			//==== Кислота ==========
			window.calc.do_acid = ( typeof window.calc.cost_of_acid == 'undefined') ? 0 : window.calc.cost_of_acid;
			jQuery('#do_acid').val(  window.calc.do_acid.toFixed(2));
			window.calc.do_total_cost_year += window.calc.do_acid;
			//==== Коагулянт ==========
			window.calc.do_coagulant = ( typeof window.calc.cost_of_coagulant == 'undefined') ? 0 : window.calc.cost_of_coagulant;
			jQuery('#do_coagulant').val(  window.calc.do_coagulant.toFixed(2));
			window.calc.do_total_cost_year += window.calc.do_coagulant;
			//==== Дозирование озона или УФ ==========
			window.calc.do_dosing_of_ozone_or_UV = 
			(( typeof window.calc.Ozonation_or_UV_rated_load == 'undefined') 
				|| ( typeof window.calc.Electricity_for_ozonation_or_UV == 'undefined') 
				|| ( typeof window.calc.Those_Operation_of_ozone_or_UV == 'undefined') 
			) ? 0 : ( window.calc.Ozonation_or_UV_rated_load * window.calc.Electricity_for_ozonation_or_UV * window.calc.Those_Operation_of_ozone_or_UV * 365);
			jQuery('#do_dosing_of_ozone_or_UV').val(  window.calc.do_dosing_of_ozone_or_UV.toFixed(2));
			window.calc.do_total_cost_year += window.calc.do_dosing_of_ozone_or_UV;		
			jQuery('#do_total_cost_year').val(  window.calc.do_total_cost_year.toFixed(2));
		}

		function calc_after_ustanovki(){
			window.calc.after_total_cost_year = 0;
			//==== Производительность насоса ==========
			window.calc.after_pump_output = ( typeof window.calc.pump_output == 'undefined') ? 0 : window.calc.pump_output;
			jQuery('#after_pump_output').val(  window.calc.after_pump_output.toFixed(2));

			//==== Частота обратной промывки в неделю ==========
			window.calc.after_backwashing_per_week = (( typeof window.calc.backwashing_per_week == 'undefined') || ( typeof window.calc.the_reduction_in_the_frequency_of_reverse_flushing == 'undefined')  || ( window.calc.the_reduction_in_the_frequency_of_reverse_flushing == 0 ) ) ? 0 : window.calc.backwashing_per_week / window.calc.the_reduction_in_the_frequency_of_reverse_flushing;
			jQuery('#after_backwashing_per_week').val( window.calc.after_backwashing_per_week.toFixed(2) );
			//==== Интервал между обратными промывками в неделях ==========
			window.calc.after_interval_between_back_flushes_in_weeks = (( typeof window.calc.after_backwashing_per_week == 'undefined') || ( window.calc.after_backwashing_per_week == 0 ) ) ? 0 : ( 1 / window.calc.after_backwashing_per_week );
			jQuery('#after_interval_between_back_flushes_in_weeks').val(  window.calc.after_interval_between_back_flushes_in_weeks.toFixed(2));
			//==== Частота обратных промывок / год ==========
			window.calc.after_return_the_frequency_of_washes = ( typeof window.calc.after_interval_between_back_flushes_in_weeks == 'undefined') ? 0 : (52 / window.calc.after_interval_between_back_flushes_in_weeks );
			jQuery('#after_return_the_frequency_of_washes').val(  window.calc.after_return_the_frequency_of_washes.toFixed(2));
			//==== Время обратной промывки (мин) ==========
			window.calc.after_time_of_backwash = ( ( typeof window.calc.time_of_backwash == 'undefined') || ( typeof window.calc.reducing_the_time_a_reverse_leaching == 'undefined')  || ( window.calc.reducing_the_time_a_reverse_leaching == 0 ) ) ? 0 : (window.calc.time_of_backwash / window.calc.reducing_the_time_a_reverse_leaching);
			jQuery('#after_time_of_backwash').val(  window.calc.after_time_of_backwash.toFixed(2) );
			//==== M³ на обратную промывку ==========
			window.calc.after_m3_for_back_washing = (( typeof window.calc.after_pump_output == 'undefined') || ( typeof window.calc.after_time_of_backwash == 'undefined') ) ? 0 : (window.calc.after_pump_output * window.calc.after_time_of_backwash);
			jQuery('#after_m3_for_back_washing').val(  window.calc.after_m3_for_back_washing.toFixed(2));
			//==== Подогрев воды для обратной промывки ==========
			
			window.calc.after_heating_water_for_backwashing = 
			(( typeof window.calc.after_m3_for_back_washing == 'undefined') 
				|| ( typeof window.calc.after_return_the_frequency_of_washes == 'undefined') 
				|| ( typeof window.calc.cost_of_electricity == 'undefined') 
			) ? 0 : ( window.calc.after_m3_for_back_washing * 1000000 * ( ( ( typeof window.calc.pool_temperature == 'undefined') ? 0 : window.calc.pool_temperature )  - ( ( typeof window.calc.temperature_of_the_feedwater == 'undefined') ? 0 : window.calc.temperature_of_the_feedwater) ) * window.calc.after_return_the_frequency_of_washes * window.calc.cost_of_electricity / 859845.2  );
			jQuery('#after_heating_water_for_backwashing').val(  window.calc.after_heating_water_for_backwashing.toFixed(2));
			
			window.calc.after_total_cost_year += window.calc.after_heating_water_for_backwashing;
			//==== Нормированная вода (водоснабжение) ==========
			window.calc.afre_normalized_water = 
			(( typeof window.calc.after_m3_for_back_washing == 'undefined') 
				|| ( typeof window.calc.cost_of_woter == 'undefined') 
				|| ( typeof window.calc.after_return_the_frequency_of_washes == 'undefined') 
			) ? 0 : ( window.calc.after_m3_for_back_washing * window.calc.cost_of_woter * window.calc.after_return_the_frequency_of_washes);
			jQuery('#afre_normalized_water').val(  window.calc.afre_normalized_water.toFixed(2));
			window.calc.after_total_cost_year += window.calc.afre_normalized_water;
			//==== Расходы на канализационно_очистительную систему ==========
			window.calc.after_cost_of_the_sewer_cleaning_system = 
			(( typeof window.calc.after_m3_for_back_washing == 'undefined') 
				|| ( typeof window.calc.cost_of_the_sewer_cleaning_system == 'undefined') 
				|| ( typeof window.calc.after_return_the_frequency_of_washes == 'undefined') 
			) ? 0 : ( window.calc.after_m3_for_back_washing * window.calc.cost_of_the_sewer_cleaning_system * window.calc.after_return_the_frequency_of_washes);
			jQuery('#after_cost_of_the_sewer_cleaning_system').val(  window.calc.after_cost_of_the_sewer_cleaning_system.toFixed(2));
			window.calc.after_total_cost_year += window.calc.after_cost_of_the_sewer_cleaning_system;
			//==== Хлор ==========
			window.calc.after_chlorine = ( typeof window.calc.cost_of_chlorine == 'undefined') ? 0 : (window.calc.cost_of_chlorine * 0.66);
			jQuery('#after_chlorine').val(  window.calc.after_chlorine.toFixed(2));
			window.calc.after_total_cost_year += window.calc.after_chlorine;
			//==== Кислота ==========
			window.calc.after_acid = ( typeof window.calc.cost_of_acid == 'undefined') ? 0 : (window.calc.cost_of_acid / 3);
			jQuery('#after_acid').val(  window.calc.after_acid.toFixed(2));
			window.calc.after_total_cost_year += window.calc.after_acid;
			//==== Коагулянт ==========
			window.calc.after_coagulant = ( typeof window.calc.cost_of_coagulant == 'undefined') ? 0 : (window.calc.cost_of_coagulant * 0) ;
			jQuery('#after_coagulant').val(  window.calc.after_coagulant.toFixed(2));
			window.calc.after_total_cost_year += window.calc.after_coagulant;
			//==== Дозирование озона или УФ ==========
			window.calc.after_dosing_of_ozone_or_UV = 
			(( typeof window.calc.Ozonation_or_UV_rated_load == 'undefined') 
				|| ( typeof window.calc.Electricity_for_ozonation_or_UV == 'undefined') 
				|| ( typeof window.calc.Those_Operation_of_ozone_or_UV == 'undefined') 
			) ? 0 : ( window.calc.Ozonation_or_UV_rated_load * window.calc.Electricity_for_ozonation_or_UV * window.calc.Those_Operation_of_ozone_or_UV * 365 *0);
			jQuery('#after_dosing_of_ozone_or_UV').val(  window.calc.after_dosing_of_ozone_or_UV.toFixed(2));

			window.calc.after_total_cost_year += window.calc.after_dosing_of_ozone_or_UV;		
			jQuery('#after_total_cost_year').val(  window.calc.after_total_cost_year.toFixed(2));
		}	
			
		function calc_start(){	
			calc_get_data();
			calc_do_ustanovki();
			calc_after_ustanovki();
			jQuery('#total_savings').val(   (window.calc.do_total_cost_year - window.calc.after_total_cost_year ).toFixed(2) );
			console.log('is ok');
		}
		
		
		console.log('calc_start');
		calc_start();
		jQuery('#hflow_calculetor_input_data input').on('change',calc_start);
		console.log('all load');
		jQuery('#hflow_calculetor_calculation_start').on('click',function(){
			jQuery(this).addClass('dms-hidden').next().removeClass('dms-hidden').parent().next('#send_data').removeClass('dms-hidden');
		});
		jQuery('#hflow_calculetor_send').on('click',function(){
			jQuery(this).addClass('dms-hidden').next().removeClass('dms-hidden');
		});
		jQuery('#hflow_submit').on('click',function(){
			jQuery(this).addClass("dms-louded").prop('disabled', true);
			var $repport_conteiner = jQuery("#hflow_calculetor #form-report").empty();
			$form = jQuery(this).parents('#hflow_calculetor').eq(0);
			// var data = $form.serialize()+"&_wpcf7_is_ajax_call=1";
			var data = $form.serialize();
			jQuery.ajax({ 
				type: 'POST',
				url: '/', 
				dataType: 'json',
				data: data,
				success: function(data){
					data.msg.forEach(function(item){
						$repport_conteiner.append( jQuery('<p class="'+item.status+'">'+item.msg+'</p>') );
					});
					jQuery('#hflow_submit').removeClass("dms-louded").prop('disabled', false);
				},
				error: function(data){
					$repport_conteiner.append( jQuery('<p class="error">Send error. host is not response</p>') );					
					jQuery('#hflow_submit').removeClass("dms-louded").prop('disabled', false);
				},
			});			
			return false;
		});
	});
})( jQuery );	