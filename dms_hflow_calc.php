<?php
/*
Plugin Name: h-flow calculator
Plugin URI: http://it-star.in.ua
Description: Калькулятор расчета економии использования СИСТЕМА ВОДОПОДГОТОВКИ ГИДРОФЛОУ http://h-flow.com.ua/ . Для запуска калькулятора, установите на странице короткий код [dms-hflow-calc]
Version: 2.0.1
Author: DStaroselskiy
Author URI: https://plus.google.com/u/0/110295925295050770002/posts
*/

define("HFLOW_CALCULATOR_DIR", plugin_dir_path( __FILE__ ), true);
define("HFLOW_CALCULATOR_URL", plugin_dir_url( __FILE__ ), true);

if( !class_exists( '\DStaroselskiy\HELLO' ) ) require_once( HFLOW_CALCULATOR_DIR."/include/class_HELLO.php");
if( !class_exists( '\DStaroselskiy\inc\ADMIN_NOTICES' ) ) require_once( HFLOW_CALCULATOR_DIR."/include/class_ADMIN_NOTICES.php");
if( !class_exists( '\DStaroselskiy\Plugins\HFLOW_CALCULATOR' ) ) require_once( HFLOW_CALCULATOR_DIR."/include/class_HFLOW_CALCULATOR.php");
if( !class_exists( '\DStaroselskiy\Shortcodes\SHORTCODE_HFLOW_CALC.php' ) ) require_once( HFLOW_CALCULATOR_DIR."/include/shortcodes/class_SHORTCODE_HFLOW_CALC.php");

global $DMS_h_flow_calculator;
$DMS_h_flow_calculator = new \DStaroselskiy\Plugins\HFLOW_CALCULATOR();

?>